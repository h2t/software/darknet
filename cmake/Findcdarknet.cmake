# Define interface variables
set(cdarknet_FOUND TRUE)
set(cdarknet_PATH ${PROJECT_SOURCE_DIR}/extern/cdarknet)
set(cdarknet_INCLUDE_DIRS ${PROJECT_SOURCE_DIR}/extern/cdarknet_include)

# Create symlinks to disambiguate includes
execute_process(
    COMMAND
        ln -fs ${cdarknet_PATH}/include/darknet.h darknet.h
    WORKING_DIRECTORY
        ${cdarknet_INCLUDE_DIRS}/cdarknet
)
execute_process(
    COMMAND
        ln -fsn ${cdarknet_PATH}/src/ darknet
    WORKING_DIRECTORY
        ${cdarknet_INCLUDE_DIRS}/cdarknet
)

# Make sure that the git submodule was initialised by probing for the .git file
if(NOT EXISTS ${cdarknet_PATH}/.git)
    message(FATAL_ERROR "git submodule for darknet not initialised.")
endif()

# Define command to call make whenever the configuration changes or the lib file does not exist
add_custom_command(
    OUTPUT
        ${cdarknet_PATH}/libdarknet.a
    COMMAND
        make --always-make GPU=$<BOOL:${darknet_GPU}> CUDNN=$<BOOL:${darknet_CUDNN}>
            OPENCV=$<BOOL:${darknet_OPENCV}> OPENMP=$<BOOL:${darknet_OPENMP}>
            DEBUG=${darknet_DEBUG}
    WORKING_DIRECTORY
        ${cdarknet_PATH}
    COMMENT
        "Building cdarknet"
)

# Define target that depends on the shared object (so it will be built)
add_custom_target(cdarknet_make
    DEPENDS
        ${cdarknet_PATH}/libdarknet.a
)

# Define command to call make in case the source files changed
add_custom_command(
    TARGET
        cdarknet_make
    PRE_BUILD
    COMMAND
        # The pipes "| grep -v 'Nothing to be done' | cat" are a workaround
        # (QtCreator interprets that output es error)
        make | grep -v 'Nothing to be done' | cat
    WORKING_DIRECTORY
        ${cdarknet_PATH}
    COMMENT
        "Calling make in cdarknet"
)

# Define a dummy target which can be depended upon.
add_library(cdarknet::cdarknet STATIC IMPORTED)
set_target_properties(cdarknet::cdarknet
    PROPERTIES
        IMPORTED_LOCATION ${cdarknet_PATH}/libdarknet.a
        INTERFACE_INCLUDE_DIRECTORIES ${cdarknet_INCLUDE_DIRS})
add_dependencies(cdarknet::cdarknet cdarknet_make)
