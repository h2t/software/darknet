/*
 * darknet C++ Wrapper
 * Copyright (C) 2019 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#include <darknet.h>


int
main (int /* argc */, char** /* argv[] */)
{
    const std::string cfgfile = "extern/cdarknet/cfg/yolov3.cfg";
    const std::string weightfile = "extern/cdarknet/data/coco.names";

    darknet::image im{1920, 1080, 3};
    darknet::network network{cfgfile, weightfile};

    network.class_count(15);
    network.set_batch(1);
    network.predict(im);
}
