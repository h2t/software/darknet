/*
 * darknet C++ Wrapper
 * Copyright (C) 2019 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#include <darknet/network.h>


// STD/STL
#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

// C darknet
#include <cdarknet/darknet.h>

// darknet
#include <darknet/image.h>


/**
 * @brief Cheshire cat for ::network (defined in C darknet darknet.h)
 */
class cdarknet::network :
    public ::network
{

    public:

        network(const cdarknet::network& network) : ::network(network) {}
        network(cdarknet::network&& network) : ::network(network) {}
        network(const ::network& network) : ::network(network) {}
        network(::network&& network) : ::network(network) {}

        ~network()
        {
            ::free_network(this);
        }

};


darknet::network::network() :
    m_network{nullptr}
{
    // pass
}


darknet::network::network(const std::string& cfgfile, const std::string& weightfile, int clear)
{
    load(cfgfile, weightfile, clear);
}


darknet::network::~network()
{
    // pass
}


void
darknet::network::load(const std::string& cfgfile, const std::string& weightfile, int clear)
{
    const int prng_seed = 2222222;
    ::srand(prng_seed);

    // std::string to char*.
    std::vector<char> cfgfile_char(cfgfile.begin(), cfgfile.end());
    std::vector<char> weightfile_char(weightfile.begin(), weightfile.end());
    cfgfile_char.push_back('\0');
    weightfile_char.push_back('\0');

    m_network = std::make_unique<cdarknet::network>(
        *::load_network(cfgfile_char.data(), weightfile_char.data(), clear));
}


bool
darknet::network::is_loaded() const
{
    return m_network != nullptr;
}


void
darknet::network::set_batch(int b)
{
    ::set_batch_network(*this, b);
}


void
darknet::network::thresh(float value)
{
    m_thresh = value;
}


float
darknet::network::thresh()
{
    return m_thresh;
}


void
darknet::network::hier_thresh(float value)
{
    m_hier_thresh = value;
}


float
darknet::network::hier_thresh()
{
    return m_hier_thresh;
}


void
darknet::network::nms(float value)
{
    m_nms = value;
}


float
darknet::network::nms()
{
    return m_nms;
}


void
darknet::network::class_count(int value)
{
    m_class_count = value;
}


int
darknet::network::class_count() const
{
    return m_class_count;
}


std::vector<darknet::detection>
darknet::network::predict(const darknet::image& input_image) const
{
    std::vector<darknet::detection> predictions;

    // Run predictions on a letterboxed copy of the input image.
    ::network_predict(*this, input_image.letterbox(m_network->w, m_network->h));

    // Get darknet detections
    int nboxes = 0;
    const bool relative = true;
    ::detection* dets = ::get_network_boxes(*this, input_image.width(), input_image.height(),
        m_thresh, m_hier_thresh, /* map = */ nullptr, relative, &nboxes);

    // Reserve memory now that we know that we have at most nboxes elements.
    predictions.reserve(static_cast<unsigned int>(nboxes));

    // Remove duplicates.
    if (m_nms > 0)
        ::do_nms_sort(dets, nboxes, m_class_count, m_nms);

    // Iterate dets and populate the output.
    for (int i = 0; i < nboxes; ++i)
    {
        darknet::detection prediction;

        // Bounding box.
        prediction.x(dets[i].bbox.x);
        prediction.y(dets[i].bbox.y);
        prediction.w(dets[i].bbox.w);
        prediction.h(dets[i].bbox.h);

        // Iterate over each probablity value for each available class.  If the probability is
        // higher than the threshold, add class candidate to output vector.
        for (int class_index = 0; class_index < m_class_count; ++class_index)
            if (dets[i].prob[class_index] > m_thresh)
                prediction.add_candidate(class_index, dets[i].prob[class_index]);

        if (prediction.candidates().size() > 0)
            predictions.push_back(std::move(prediction));
    }

    // Free memory by shrinking vector now that all objects were populated.
    predictions.shrink_to_fit();

    // Free memory occupied by Darknet.
    ::free_detections(dets, nboxes);

    return predictions;
}


darknet::network::operator cdarknet::network*()
{
    return m_network.get();
}


darknet::network::operator cdarknet::network*() const
{
    return m_network.get();
}
