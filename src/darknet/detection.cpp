/*
 * darknet C++ Wrapper
 * Copyright (C) 2019 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#include <darknet/detection.h>


// STD/STL
#include <map>


darknet::detection::detection()
{
    // pass
}


darknet::detection::~detection()
{
    // pass
}


void
darknet::detection::add_candidate(int class_index, float certainty)
{
    m_candidates[class_index] = certainty;
}


std::map<int, float>
darknet::detection::candidates() const
{
    return m_candidates;
}


void
darknet::detection::x(float value)
{
    m_x = value;
}


float
darknet::detection::x() const
{
    return m_x;
}


void
darknet::detection::y(float value)
{
    m_y = value;
}


float
darknet::detection::y() const
{
    return m_y;
}


void
darknet::detection::w(float value)
{
    m_w = value;
}


float
darknet::detection::w() const
{
    return m_w;
}


void
darknet::detection::h(float value)
{
    m_h = value;
}


float
darknet::detection::h() const
{
    return m_h;
}
