/*
 * darknet C++ Wrapper
 * Copyright (C) 2019 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#include <darknet/image.h>


// C darknet
#include <cdarknet/darknet.h>


/**
 * @brief Cheshire cat for ::image (defined in C darknet darknet.h).
 */
class cdarknet::image :
    public ::image
{

    public:

        image(const cdarknet::image& image) : ::image{image} {}
        image(cdarknet::image&& image) : ::image{image} {}
        image(const ::image& image) : ::image{image} {}
        image(::image&& image) : ::image{image} {}

};


darknet::image::image(cdarknet::image&& image) :
    m_image{std::make_unique<cdarknet::image>(std::move(image))}
{
    // pass
}


darknet::image::image()
{
    // pass
}


darknet::image::image(const darknet::image& image) :
    m_image{std::make_unique<cdarknet::image>(*image.m_image.get())}
{
    // pass
}


#ifdef OPENCV
darknet::image::image(const ::IplImage& image)
{
    int h = image.height;
    int w = image.width;
    int c = image.nChannels;
    ::image im = ::make_image(w, h, c);
    int step = image.widthStep;

    for (int i = 0; i < h; ++i)
        for (int k = 0; k < c; ++k)
            for (int j = 0; j < w; ++j)
                im.data[k * w * h + i * w + j] =
                    static_cast<unsigned char>(image.imageData[i * step + j * c + k]) / 255.f;

    m_image = std::make_unique<cdarknet::image>(std::move(im));
    ::rgbgr_image(*this);
}
#endif


#ifdef OPENCV
darknet::image::image(const cv::Mat& image) :
    darknet::image{static_cast<::IplImage>(image)}
{
    // pass
}
#endif


darknet::image::image(int width, int height, int channels) :
    darknet::image{::make_image(width, height, channels)}
{
    // pass
}


darknet::image::~image()
{
    ::free_image(*m_image);
}


int
darknet::image::width() const
{
    return m_image->w;
}


int
darknet::image::height() const
{
    return m_image->h;
}


darknet::image
darknet::image::letterbox(int width, int height) const
{
    return darknet::image{::letterbox_image(*this, width, height)};
}


darknet::image::operator cdarknet::image() const
{
    return *m_image.get();
}


darknet::image::operator float*() const
{
    return m_image->data;
}
