/*
 * darknet C++ Wrapper
 * Copyright (C) 2019 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#pragma once


// darknet
#include <darknet/image.h>
#include <darknet/detection.h>
#include <darknet/network.h>


namespace darknet
{


/**
 * @brief Makes a unique color for a given object class index and total object count.
 * @param class_index Object class index.
 * @param class_count Total object count.
 * @return RGB color as 3-tuple.
 */
std::tuple<int, int, int> get_color(int class_index, int class_count);


#ifdef GPU
void cuda_set_device(int id);
#endif


}
