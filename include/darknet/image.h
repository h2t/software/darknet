/*
 * darknet C++ Wrapper
 * Copyright (C) 2018 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <memory>

// OpenCV
#ifdef OPENCV
#include <opencv2/core/core.hpp>
#endif


namespace cdarknet
{

class image;

}


namespace darknet
{


class image
{

    private:

        std::unique_ptr<cdarknet::image> m_image;

        image(cdarknet::image&& image);

    public:

        image();
        image(const darknet::image& image);

#ifdef OPENCV
        image(const ::IplImage& image);
        image(const cv::Mat& image);
#endif

        image(int width, int height, int channels);
        virtual ~image();

        int width() const;
        int height() const;

        darknet::image letterbox(int width, int height) const;

        operator cdarknet::image() const;
        operator float*() const;

};


}
